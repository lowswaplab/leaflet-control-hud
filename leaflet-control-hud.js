
// Leaflet Head-Up Display (HUD) Control

// https://gitlab.com/lowswaplab/leaflet-control-hud

// Copyright © 2020 Low SWaP Lab lowswaplab.com

// https://fontawesome.com/icons?d=gallery&m=free
import "@fortawesome/fontawesome-free/css/all.min.css";

// https://geographiclib.sourceforge.io/html/js/
import GeographicLib from "geographiclib";

(function()
  {
  L.Control.HUD = L.Control.extend(
    {
    _this: this,
    _divTitle: undefined,
    _divDatetime: undefined,
    _divCenter: undefined,
    _divMouse: undefined,
    _divRange: undefined,
    _buttonSettings: undefined,
    _divSettings: undefined,
	_posMouse: undefined,
    _posCenter: undefined,
    _geod: GeographicLib.Geodesic.WGS84,
    _cmDistance: undefined,   // distance between center and mouse meters
    _cmAzimuth: undefined,    // azimuth from center to mouse degrees true north
    _cmAzimuthInv: undefined, // azimuth from mouse to center degrees true north
    _selectFormatPosition: undefined,
    _selectUnitsDistance: undefined,
    _selectUnitsElevation: undefined,

	options:
      {
      terrainEl: undefined,
      position: "bottomleft",
      style: "padding:4px; color:#000; background-color:#FFF;" +
             "box-shadow 0 1px 7px rgba(0, 0, 0, 0.4);" +
             "border-radius:5px; border:2px solid rgba(0, 0, 0, 0,2);" +
             "cursor:text;",
      title: "",

      // dd (decimal degrees)
      // dm (degrees, decimal minutes)
      // dms (degrees, minutes, seconds)
      formatPosition: "dd",

      // metric (m, km)
      // USCS (US customary system; yards, miles)
      // nautical (yards, nautical miles)
      unitsDistance: "metric",

      // meters
      // feet (US customary system)
      unitsElevation: "meters"
      },

    initialize: function(options)
      {
      L.setOptions(this, options);
      },

	onAdd: function (map)
      {
      var _this = this;
      var divDatetimeMain;
      var divCenterMain;
      var divMouseMain;
      var divRangeMain;

      this._container = L.DomUtil.create("div", "leaflet-control-hud");

      this._divTitle = L.DomUtil.create("div");
      this._divTitle.id = "leaflet-control-hud-title";
      this._divTitle.style.whiteSpace = "nowrap";
      this._divDatetime = L.DomUtil.create("span");
      this._divDatetime.id = "leaflet-control-hud-datetime";
      this._divCenter = L.DomUtil.create("span");
      this._divCenter.id = "leaflet-control-hud-center";
      this._divMouse = L.DomUtil.create("span");
      this._divMouse.id = "leaflet-control-hud-mouse";
      this._divRange = L.DomUtil.create("span");
      this._divRange.id = "leaflet-control-hud-range";

      divDatetimeMain = L.DomUtil.create("div");
      divDatetimeMain.style.whiteSpace = "nowrap";
      divDatetimeMain.innerHTML = "<span class='far fa-fw fa-clock'></span> ";
      divDatetimeMain.appendChild(this._divDatetime);

      divCenterMain = L.DomUtil.create("div");
      divCenterMain.style.whiteSpace = "nowrap";
      divCenterMain.innerHTML = "<span class='fas fa-fw fa-plus'></span> ";
      divCenterMain.appendChild(this._divCenter);

      divMouseMain = L.DomUtil.create("div");
      divMouseMain.style.whiteSpace = "nowrap";
      divMouseMain.innerHTML =
        "<span class='fas fa-fw fa-mouse-pointer'></span> ";
      divMouseMain.appendChild(this._divMouse);

      divRangeMain = L.DomUtil.create("div");
      divRangeMain.style.whiteSpace = "nowrap";
      divRangeMain.innerHTML = "<span class='fas fa-fw fa-ruler'></span> ";
      divRangeMain.appendChild(this._divRange);

      this._buttonSettings = L.DomUtil.create("div");
      this._buttonSettings.innerHTML =
        "<span class='fas fa-fw fa-cog'></span> ";
      this._buttonSettings.style.cssFloat = "right";
      this._buttonSettings.style.cursor = "pointer";
      this._buttonSettings.style.display = "none";
      this._buttonSettings.onclick = function()
        {
        _this._clickSettings();
        };
      divRangeMain.appendChild(this._buttonSettings);

      this._divSettings = L.DomUtil.create("div");
      this._divSettings.innerHTML =
        "<hr style='margin: 2px 2px 2px 2px'> \
        <div style='font-style:bold;font-size:1.25em;'> \
          <span class='fas fa-fw fa-cog'></span> Settings \
        </div> \
        <table> \
           <tr> \
             <th colspan='3' style='text-align:left'>Format</td> \
           </tr> \
           <tr> \
             <td></td> \
             <td>Position</td> \
             <td> \
               <select id='leaflet-control-hud-formatPosition'> \
                 <option value='dd'>Decimal degrees</option> \
                 <option value='dm'>Degrees, decimal minutes</option> \
                 <option value='dms'>Degrees, minutes, seconds</option> \
               </select> \
             </td> \
           </tr> \
           <tr> \
             <th colspan='3' style='text-align:left'>Units</td> \
           </tr> \
           <tr> \
             <td></td> \
             <td>Distance</td> \
             <td> \
               <select id='leaflet-control-hud-unitsDistance'> \
                 <option value='metric'>Metric (m, km)</option> \
                 <option value='USCS'>US (ft, yd, mi)</option> \
                 <option value='nautical'>Nautical (ft, yd, NM)</option> \
               </select> \
             </td> \
           </tr> \
           <tr> \
             <td></td> \
             <td>Elevation</td> \
             <td> \
               <select id='leaflet-control-hud-unitsElevation'> \
                 <option value='meters'>meters</option> \
                 <option value='feet'>feet</option> \
               </select> \
             </td> \
           </tr> \
         </table>";
      this._divSettings.style.display = "none";

      this._container.appendChild(this._divTitle);
      this._container.appendChild(divDatetimeMain);
      this._container.appendChild(divCenterMain);
      this._container.appendChild(divMouseMain);
      this._container.appendChild(divRangeMain);
      this._container.appendChild(this._divSettings);

      L.DomEvent.disableClickPropagation(this._container);
      map.on("mousemove", this._onMouseMove, this);
      map.on("zoomend", this._onZoomEnd, this);
      this._container.style = this.options.style;

      this._container.addEventListener("mouseenter", function()
        {
        _this._onMouseEnter();
        });
      this._container.addEventListener("mouseleave", function()
        {
        _this._onMouseLeave();
        });
      map._container.addEventListener("mouseleave", function()
        {
        _this._onMouseLeaveMap();
        });

      this._posCenter = this.setElevation(this._map.getCenter().wrap());

      // don't call _update() immediately because we have to wait
      // for all our divs n stuff to be created and put in place
      setInterval(function()
        {
        _this._update(_this);
        },
        1000);

      return(this._container);
      },

	onRemove: function (map)
      {
      map.off("mousemove", this._onMouseMove)
      // XXX stop setInterval()
      },

    _update: function(obj=undefined)
      {
      var dt;
      var year;
      var month;
      var date;
      var hours;
      var minutes;
      var seconds;
      var zone;
      var zoneHours;
      var zoneMinutes;
      var zonestr;

      // this is for setInterval() and addEventListener()
      if (obj === undefined)
        obj = this;

      dt = new Date();

      year = dt.getFullYear();
      month = obj._leadingZero(dt.getMonth() + 1);
      date = obj._leadingZero(dt.getDate());
      hours = obj._leadingZero(dt.getHours());
      minutes = obj._leadingZero(dt.getMinutes());
      seconds = obj._leadingZero(dt.getSeconds());
      // XXX test with negative offsets
      zone = dt.getTimezoneOffset();
      zoneHours = obj._leadingZero(Math.floor(zone/60));
      zoneMinutes = obj._leadingZero(zone - zoneHours*60);
      zonestr = zoneHours + ":" + zoneMinutes;
      if (zone >= 0)
        zonestr = "UTC-" + zonestr;
      else
        zonestr = "UTC-" + zonestr;

      if (obj.options.title !== undefined)
        if (obj.options.title.length > 0)
          obj._divTitle.innerHTML = obj.options.title;

      obj._divDatetime.innerHTML = year + "-" + month + "-" + date + " " +
        hours + ":" + minutes + ":" + seconds + " (" + zonestr + ")";

      if (obj._posCenter === undefined)
        {
        obj._divCenter.innerHTML = "";
        }
      else
        {
        obj._divCenter.innerHTML = obj._positionStr(obj._posCenter);
        };

      if (obj._posMouse === undefined)
        {
        obj._divMouse.innerHTML = "";
        }
      else
        {
        obj._divMouse.innerHTML = obj._positionStr(obj._posMouse);
        };

      if (obj._cmDistance === undefined || obj._cmAzimuth === undefined ||
          obj._cmAzimuthInv === undefined)
        {
        obj._divRange.innerHTML = "";
        }
      else
        {
        obj._divRange.innerHTML = obj._distanceStr(obj._cmDistance) + " " +
          obj._cmAzimuth + "°T ("+ obj._cmAzimuthInv + "°T)";
        }

      // show difference in elevation
      if (obj._posCenter !== undefined && obj._posMouse !== undefined)
        {
        if (!isNaN(obj._posCenter.alt) && !isNaN(obj._posMouse.alt))
          {
          var diff;

          diff = obj._posMouse.alt - obj._posCenter.alt;
          obj._divRange.innerHTML += " " + obj._elevationStr(diff.toFixed(0));
          };
        };
      },

    dd2dm(deg)
      {
      var dm={};
      var minf;

      dm.degrees = Math.floor(deg);
      dm.minutes = Math.abs(dm.degrees - deg) * 60;

      return(dm);
      },
    dd2dms(deg)
      {
      var dms={};
      var minf;

      dms.degrees = Math.floor(deg);
      minf = Math.abs(dms.degrees - deg) * 60;
      dms.minutes = Math.floor(minf);
      dms.seconds = (minf - dms.minutes) * 60;

      return(dms);
      },

    dms2dd(deg, min, sec)
      {
      var dd;

      dd = Math.abs(Math.floor(deg)) + (min/60) + (sec/3600);
      if (degrees < 0)
        dd *= -1;

      return(dd);
      },

    getFormatPosition: function()
      {
      var index;

      if (this._selectFormatPosition === undefined)
        {
        this._selectFormatPosition =
          document.getElementById("leaflet-control-hud-formatPosition");
//console.dir(this._selectFormatPosition);
        this.setFormatPosition(this.options.formatPosition);
        };

      index = this._selectFormatPosition.selectedIndex;

      return(this._selectFormatPosition.options[index].value);
      },

    getUnitsDistance: function()
      {
      var index;

      if (this._selectUnitsDistance === undefined)
        {
        this._selectUnitsDistance =
          document.getElementById("leaflet-control-hud-unitsDistance");
        this.setUnitsDistance(this.options.unitsDistance);
        };

      index = this._selectUnitsDistance.selectedIndex;

      return(this._selectUnitsDistance.options[index].value);
      },

    getUnitsElevation: function(obj)
      {
      var index;

      if (this._selectUnitsElevation === undefined)
        {
        this._selectUnitsElevation =
          document.getElementById("leaflet-control-hud-unitsElevation");
        this.setUnitsElevation(this.options.unitsElevation);
        };

      index = this._selectUnitsElevation.selectedIndex;

      return(this._selectUnitsElevation.options[index].value);
      },

    /* set elevation, if available of given latlng */
    setElevation: function(latlng)
      {
      if (this.options.terrainEl)
        latlng.alt = this.options.terrainEl.getElevation(latlng);

      return(latlng);
      },

    setFormatPosition: function(units)
      {
      this._selectFormatPosition.value = units;
      },

    setUnitsDistance: function(units)
      {
      this._selectUnitsDistance.value = units;
      },

    setUnitsElevation: function(units)
      {
      this._selectUnitsElevation.value = units;
      },

    _clickSettings: function()
      {
      if (this._divSettings.style.display === "none")
        {
        this._buttonSettings.innerHTML =
          "<span class='fas fa-fw fa-times'></span> ";
        this._divSettings.style.display = "block";
        }
      else
        {
        this._buttonSettings.style.display = "none";
        this._divSettings.style.display = "none";
        this._buttonSettings.innerHTML =
          "<span class='fas fa-fw fa-cog'></span> ";
        };
      },

    _onMouseEnter: function(evt)
      {
      this._buttonSettings.style.display = "block";
      },

    _onMouseLeave: function(evt)
      {
      this._buttonSettings.style.display = "none";
      },

    _onMouseLeaveMap: function(evt)
      {
      this._posMouse = undefined;
      this._cmDistance = undefined;
      this._cmAzimuth = undefined;
      this._cmAzimuthInv = undefined;
      this._update();
      },

	_onMouseMove: function(evt)
      {
      var inv;

      this._onZoomEnd(evt); // to make sure _posCenter is set

      this._posMouse = this.setElevation(evt.latlng.wrap());

      inv = this._geod.Inverse(this._posCenter.lat, this._posCenter.lng,
        this._posMouse.lat, this._posMouse.lng);
      this._cmDistance = Math.round(inv.s12);
      this._cmAzimuth = Math.round(inv.azi1);
      if (this._cmAzimuth < 0)
        this._cmAzimuth = 360 + this._cmAzimuth;
      this._cmAzimuthInv = (this._cmAzimuth + 180) % 360;

      this._update();
      },

    _onZoomEnd: function(evt)
      {
      this._posCenter = this.setElevation(this._map.getCenter().wrap());
      },

    _leadingZero: function(num)
      {
      if (num <= 9)
        return("0" + num);
      return(num);
      },

    _positionStr: function(pos)
      {
      var format;
      var text;

      format = this.getFormatPosition();

      if (format === "dm")
        {
        var dm;

        dm = this.dd2dm(pos.lat);
        text = dm.degrees + "°" + dm.minutes.toFixed(3) + "' ";

        dm = this.dd2dm(pos.lng);
        text += dm.degrees + "°" + dm.minutes.toFixed(3) + "'";
        }
      else if (format === "dms")
        {
        var dms;

        dms = this.dd2dms(pos.lat);
        text = dms.degrees + "°" + dms.minutes + "'" +
          dms.seconds.toFixed(1) + '" ';

        dms = this.dd2dms(pos.lng);
        text += dms.degrees + "°" + dms.minutes + "'" +
          dms.seconds.toFixed(1) + '"';
        }
      else // dd
        {
        text = pos.lat.toFixed(5) + "° " + pos.lng.toFixed(5) + "°";
        };

      if (!isNaN(pos.alt))
        text += " " + this._elevationStr(Math.round(pos.alt));

      return(text);
      },

    _distanceStr: function(dist)
      {
      var units;

      units = this.getUnitsDistance();

      if (units === "USCS")
        {
        var miles;

        miles = dist / 1609.344;

        if (miles >= 30)
          return(miles.toFixed(0) + "mi");
        if (miles >= 1)
          return(miles.toFixed(1) + "mi");

        var yards;

        yards = dist / 0.9144;

        if (yards > 100)
          return(yards.toFixed(0) + "yd");

        return(Math.round(dist / 0.3048) + "ft");
        }
      else if (units === "nautical")
        {
        var nmiles;

        nmiles = dist / 1852;

        if (nmiles >= 30)
          return(nmiles.toFixed(0) + "NM");
        if (nmiles >= 1)
          return(nmiles.toFixed(1) + "NM");

        var yards;

        yards = dist / 0.9144;

        if (yards > 100)
          return(yards.toFixed(0) + "yd");

        return(Math.round(dist / 0.3048) + "ft");
        };

      if (dist >= 50000)
        return((dist/1000).toFixed(0) + "km")
      if (dist >= 5000)
        return((dist/1000).toFixed(1) + "km")

      return(dist + "m");
      },

    _elevationStr: function(dist)
      {
      var units;
      var str;

      units = this.getUnitsElevation();

      if (units === "feet")
        str = Math.round(dist / 0.3048) + "ft";
      else
        str = dist + "m";

      return(str);
      }
    });

  L.Map.mergeOptions(
    {
    positionControl: false
    });

  L.Map.addInitHook(function ()
    {
    if (this.options.positionControl)
      {
      this.positionControl = new L.Control.HUD();
      this.addControl(this.positionControl);
      };
    });

  L.control.HUD = function (options)
    {
    return(new L.Control.HUD(options));
    };
  }
)();

