
# leaflet-control-hud

[Leaflet](https://leafletjs.com/) Head-Up Display (HUD) Control

# NPM

This package is available on [npm](https://www.npmjs.com/):

[@lowswaplab/leaflet-control-hud](https://www.npmjs.com/package/@lowswaplab/leaflet-control-hud)

This package can be installed by performing:

    npm install @lowswaplab/leaflet-control-hud

# JavaScript

Example, with options (and defaults):

    import "@lowswaplab/leaflet-control-hud";

    L.control.HUD(
      {
      terrainEl: undefined,     // leaflet-tilelayer-terrainel layer
      style: "...",             // HUD box style (see source for default)
      title: "",                // title to display on first line of HUD
      unitsDistance: "metric",  // distance units
                                // "metric" (meters, km)
                                // "USCS" (feet, yards, miles; US customary),
                                // "nautical" (feet, yards, nautical miles)
      unitsElevation: "meters", // elevation units
                                // "meters"
                                // "feet"
      }).addTo(map);

This module can also use
[leaflet-tilelayer-terrainel](https://gitlab.com/lowswaplab/leaflet-tilelayer-terrainel)
to display terrain elevation:

    import "@lowswaplab/leaflet-tilelayer-terrainel";
    import "@lowswaplab/leaflet-control-hud";

    var te=L.tileLayer.terrainEl("https://api.mapbox.com/v4/mapbox.terrain-rgb/{z}/{x}/{y}.pngraw?access_token=" + mapboxAccessToken,
      {
      maxNativeZoom: 14,
      maxZoom: 23,
      opacity: 0
      }).addTo(map);
    L.control.HUD(
      {
      terrainEl: te
      }).addTo(map);

# Source Code

[leaflet-control-hud](https://gitlab.com/lowswaplab/leaflet-control-hud)

# Contribution

If you find this software useful, please consider financial support
for future development via
[PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GY9C5FN54TCKL&source=url).

# Author

[Low SWaP Lab](https://www.lowswaplab.com/)

# Copyright

Copyright © 2020 Low SWaP Lab [lowswaplab.com](https://www.lowswaplab.com/)

